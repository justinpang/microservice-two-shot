from django.contrib import admin
from wardrobe_api.models import Bin

# Register your models here.
@admin.register(Bin)
class BinAdmin(admin.ModelAdmin):
    pass
