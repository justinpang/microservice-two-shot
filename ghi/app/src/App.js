import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import NewShoeForm from './NewShoe';
import ShoeList from './ShoeList';
import DeleteShoe from './DeleteShoe';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes/new" element={<NewShoeForm />} />
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="/shoes/delete" element={<DeleteShoe />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
