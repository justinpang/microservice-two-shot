import React from 'react';

function ShoeList(props){
    if (props.shoes === undefined) {
        return null;
    }
    return (
    <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Bin Number</th>
                </tr>
            </thead>
            <tbody>
            {props.shoes.map(shoe => {
                return (
                    <tr key={shoe.href}>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.name }</td>
                        <td>{ shoe.color }</td>
                        <td><img src={ shoe.picture_url } height={100} width={100} alt={shoe.name} ></img></td>
                        <td>{ shoe.bin }</td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    </div>
    )
}

export default ShoeList;