from django.contrib import admin
from django.urls import path

from shoes_rest.views import api_list_shoes, api_shoe_detail

urlpatterns = [
    path("shoes/", api_list_shoes, name="create_shoe"),
    path(
        "bins/<int:bin_vo_id>/shoes/",
        api_list_shoes,
        name="api_list_shoes",
    ),
    path("shoes/<int:pk>/", api_shoe_detail, name="api_shoe_detail"),
]