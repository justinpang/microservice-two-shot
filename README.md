# Wardrobify

Team:

* Justin Pang - Shoes
* Ryan Spurlock - Hats

## Design

## Shoes microservice

Shoes API contains 2 models: BinVO and Shoe.
BinVO is the model that is used to store the data recieved from the wardrobe bin api.
Data is requested through the poller.py file that pings the bin api and makes a request for the bin data.
Once Bin data is recieved, it can be used in the shoe model.
Shoe model is used for all the required information for the shoes that will be created in the app. It contains a manufacturer,
name, picture url, color, and bin field. 

Views.py contains function views to list and create shoes to the through the shoes api. The Shoes api then contacts the react application to portray the relevant data on the front end through Shoes List, Add Shoe, and Delete Shoe.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
